# MVI - Flow - Compose

Live data does cacheing and Flow does not.


[  Compose ] --> [ ViewModel] --> Flow --> [ Room ]

Do exactly what Alex did. But with MVI.
https://github.com/alexsullivan114/KotlinEverywhereBoston/tree/convert-to-flow

---


### Simple MVI: A LiveData Based MVI Approach - Dan Lowe & Dustin Summers
Video
https://www.youtube.com/watch?v=M34NoFI1-6I

Code:
https://gitlab.com/dan-0/simplemvi/-/tree/as_presentation

---
### Complex MVI
Here is the info 3 part series (Complex)

https://medium.com/google-developer-experts/android-mvi-architecture-with-jetpack-coroutines-flow-part-1-recyclerview-adapter-w-83a10134207f

---

### Upvote

Here is the code - Flow branch. **Does not use Room or ModelView**
https://github.com/kanawish/upvote/tree/flow/app/src/main/java/com/kanawish/upvote/common

MainViewEvent -- Intent.

**View**.Click -> map { MainViewEvent.**ViewClick** }

Next we merge all the events into one stream

Currently using viewModel to store data.
data class Model()  We will use ROOM with a DAO
newState = OldState.copy( x = x + 1)

Intent -> Produces a reducer function

class AddHeart():Intent<t> {
    override fun reduce(oldstat : <T>) = { oldstate.copy(x = oldstate.x + 1)  }

}
---
### MVI with Jetpack Compose
***Just slides no code***

https://medium.com/swlh/android-mvi-with-jetpack-compose-b0890f5156ac

1) Setup ViewModelState
```
   sealed class MVIViewState {
    object Loading: MVIViewState()
    class Error(val reason: String): MVIViewState()
    class Success(val result: String): MVIViewState()
   }
```

publish through LiveData<MVIViewState>

```
class MVIViewModel : ViewModel() {
  val viewState: MutableLiveData<MVIViewState>()

  fun fetchData() {
    myDataSource.fetchData()
      .doOnSubscribe {
        viewState.postValue(MVIViewState.Loading)
      }
      .observe(
        {
          viewState.postValue(MVIViewState.Success(it.result))
        },
        {
          viewState.postValue(MVIViewState.Error(it.reason))
        }
      )
  }
```
From Activity
```
class MyActivity: Activity() {
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setupViewModel()
  }
  private fun setupViewModel() {
     viewModel = //factory fancy way to get the instance of ViewModel
     viewModel.viewState.observe(this, Observer {
       when (it) {
         MVIViewState.Loading -> { // show loading
         }
         MVIViewState.Error -> { // display error message
         }
         MVIViewState.Success -> { // show data fetched
         }
       }
     )
  }
}
```

Replace all views with @Compose and @Model.

---
How you do it

Your Presenter now has one output: the state of the View. This is done with the View’s render(), which accepts the current state of the app as an argument.

Intents in MVI represent a future action that changes the app’s state.

How you build MVI.

1. ***display{DATA}Intent***: Binds UI actions to the appropriate intents -- {Button} -> {Intent}  [MainView Object]
        * We’re using {lamba functions} RxBinding to convert button click listeners into RxJava {lamba} Observables.

1. ***render:*** Map the state of the app to the ***View*** -- {ViewState}  ->  {correct methods} [MainView Object].

```
override fun render(state: MovieState) {
    when(state) {
      is MovieState.DataState -> renderDataState(state)
      is MovieState.LoadingState -> renderLoadingState()
      is MovieState.ErrorState -> renderErrorState(state)
    }
  }
```

1. ***renderDataState*** ~~internal method~~ for updating the display based on the state.  {state} -> {UI}

a. Render a loading screen in your View  // b. Render a loading screen in your View // c ...

render() that takes and Intent and update the UI.


-- One -- one render() receives the state of your app from your View(Presenter) and an Intent triggered by a button click

###State Reducers

Reducer functions consist of two main components:

*Accumulator*: The total value accumulated so far in each iteration of your reducer function. It should be the first argument.
*Current Value*: The current value passing through each iteration of your reducer function. It should be the second argument.

What does this have to do with State Reducers and MVI?


The process works as follows:

* Create a new state called ***PartialState*** that represents new changes in your app.

* When there is a new Intent that requires a previous state of your app as a starting point, create a new PartialState rather than a complete state.

* Create a new `reduce()` function that takes the previous state and a PartialState as arguments and defines how to merge both into a new state to be displayed.

* Use RxJava `scan()` to apply `reduce()` to the initial state of your app and return the new state.

It’s up to each developer to implement a reducer function to merge the two states of the current app. Developers often use RxJava scan or merge operators to help with this task.

Developers often use RxJava scan or merge operators to help with this task.


In this tutorial, you learned the key points of MVI, including:

MVI stands for Model-View-Intent
- Models in MVI represent a state of an app.
- A state represents how an app behaves or reacts at any given moment such when loading screen or displaying new data in a list or a network error.
- Views in MVI can have one or more intent()s that handle user actions and a single render() that renders the state of your app.
- The Intent represents an intention to perform an action by the user like an API call or a new query in your database. It does not represent the usual android.content.Intent
- Reducer functions provide steps to merge things into a single component called the accumulator.
- MVI provides a unidirectional and cyclical data flow.


---
Working example

https://github.com/QArtur99/Compose-ShoppingList

1> turn user classes into a list of intents -- Kotlin Sealed Class.
2> Make an Obserabale from our Intent. If Mutli then merge them.
3> Tranform Intent for business Logic.
-- List all

Every Action is maped to an intent
Sealed Class - Every Intent -> Every Action

Action -> 3 f{} , f{} , f{} -> resault // Obervable Transformer
Check the type of action and send it to the proper function

reducer = oldstate + action = newstate(oldstate)

In code == reducer

Render Function
https://quickbirdstudios.com/blog/android-mvi-kotlin-coroutines-flow/?utm_source=reddit.com


Navigation

yeah our intention is to provide a compose Navigator for the navigation arch component. 
It was designed to not be fragment-specific. There's a lot of tooling work that we'd like to have 
around it in android studio to call it a product on par with the current experience, 
so it's probably going to be a post-compose 1.0

TBD. Probably something along the lines of @Composable (???) -> Unit  function objects as part of 
the destinations rather than fragment names or factories


